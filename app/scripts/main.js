$(document).ready(function () {

  $(window).bind('load resize scroll', function () {

    if ($(window).width() < 1199) {
      $('.cr-top__nav-toggle').click(function () {
        $(this).next().slideToggle('fast', function () {
        });
      });
    };

  });

  $('.cr-slick').slick({
    arrows: false,
    dots: true,
    autoplay: true,
    speed: 300
  });

  $('.cr-about__cards').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    speed: 300
  });

  $('.cr-reviews__cards').slick({
    arrows: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.cr-companies__cards').slick({
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.cr-top__droper').click(function () {
    $('.cr-top__nav').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.cr-collapse__head').click(function () {
    $(this).next().slideToggle('fast', function () {
      $(this).prev().parent().toggleClass('cr-collapse_dark');
      $(this).prev().find('.cr-collapse__head-arrow').toggleClass('cr-collapse__head-arrow_rotated');
      $('.cr-tips__cards').slick('setPosition');
    });
  });

});

